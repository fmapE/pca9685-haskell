{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE ExplicitForAll #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeOperators #-}
module System.PWM.PCA9685 where

import Data.ByteString.Lazy (toStrict)
import Data.ByteString.Builder (toLazyByteString, word8, word16LE)
import Data.Bits
import Data.Int
import Data.Monoid
import Data.Proxy
import Data.Word
import GHC.Exts
import GHC.TypeLits
import System.IO.I2C
import System.PWM.PCA9685.Registers

data OutputType
  = OpenDrain
  | TotemPole
  deriving (Eq, Show)

class KnownNat n => PCA9685Output n where
  onLRegister :: Word8
  onHRegister :: Word8
  offLRegister :: Word8
  offHRegister :: Word8

instance PCA9685Output 0 where
  onLRegister  = led0OnL
  onHRegister  = led0OnH
  offLRegister = led0OffL
  offHRegister = led0OffH
  
instance PCA9685Output 1 where
  onLRegister  = led1OnL
  onHRegister  = led1OnH
  offLRegister = led1OffL
  offHRegister = led1OffH
  
instance PCA9685Output 2 where
  onLRegister  = led2OnL
  onHRegister  = led2OnH
  offLRegister = led2OffL
  offHRegister = led2OffH
  
instance PCA9685Output 3 where
  onLRegister  = led3OnL
  onHRegister  = led3OnH
  offLRegister = led3OffL
  offHRegister = led3OffH
  
instance PCA9685Output 4 where
  onLRegister  = led4OnL
  onHRegister  = led4OnH
  offLRegister = led4OffL
  offHRegister = led4OffH
  
instance PCA9685Output 5 where
  onLRegister  = led5OnL
  onHRegister  = led5OnH
  offLRegister = led5OffL
  offHRegister = led5OffH
  
instance PCA9685Output 6 where
  onLRegister  = led6OnL
  onHRegister  = led6OnH
  offLRegister = led6OffL
  offHRegister = led6OffH
  
instance PCA9685Output 7 where
  onLRegister  = led0OnL
  onHRegister  = led0OnH
  offLRegister = led0OffL
  offHRegister = led0OffH
  
instance PCA9685Output 8 where
  onLRegister  = led8OnL
  onHRegister  = led8OnH
  offLRegister = led8OffL
  offHRegister = led8OffH
  
instance PCA9685Output 9 where
  onLRegister  = led9OnL
  onHRegister  = led9OnH
  offLRegister = led9OffL
  offHRegister = led9OffH
  
instance PCA9685Output 10 where
  onLRegister  = led10OnL
  onHRegister  = led10OnH
  offLRegister = led10OffL
  offHRegister = led10OffH
  
instance PCA9685Output 11 where
  onLRegister  = led11OnL
  onHRegister  = led11OnH
  offLRegister = led11OffL
  offHRegister = led11OffH
  
instance PCA9685Output 12 where
  onLRegister  = led12OnL
  onHRegister  = led12OnH
  offLRegister = led12OffL
  offHRegister = led12OffH
  
instance PCA9685Output 13 where
  onLRegister  = led14OnL
  onHRegister  = led14OnH
  offLRegister = led14OffL
  offHRegister = led14OffH
  
instance PCA9685Output 14 where
  onLRegister  = led14OnL
  onHRegister  = led14OnH
  offLRegister = led14OffL
  offHRegister = led14OffH
  
instance PCA9685Output 15 where
  onLRegister  = led15OnL
  onHRegister  = led15OnH
  offLRegister = led15OffL
  offHRegister = led15OffH

data PCA9685Config
  = PCA9685Config
  { pca9685PhaseSeparation :: Word16
  , pca9685Address :: Int64
  } deriving (Eq, Show)

type family NotYetSet (n :: Nat) (l :: [Nat]) :: Constraint where
  NotYetSet n '[] = 'True ~ 'True
  NotYetSet n (n ': ns) = 
    TypeError
    ( 'Text "Duplicated Output" :<>: 'ShowType n )
  NotYetSet n (m ': ms) = NotYetSet n ms

type family PCA9685Outputs (l :: [Nat]) :: Constraint where
  PCA9685Outputs '[] = True ~ True
  PCA9685Outputs (n : ns) = (PCA9685Output n, PCA9685Outputs ns)
  
data SetOutput (n :: Nat)
  = SetOutput Double
  deriving (Eq, Show)

to
  :: forall n
  .  PCA9685Output n
  => Double
  -> SetOutput n
to = SetOutput

data SetOutputs (outputs :: [Nat]) where
  NoOutputs :: SetOutputs '[]
  AndOutput :: SetOutput n -> SetOutputs ns -> SetOutputs (n : ns)

nil :: SetOutputs '[]
nil = NoOutputs

et
  :: forall n ns
  .  (NotYetSet n ns)
  => SetOutput n
  -> SetOutputs ns
  -> SetOutputs (n : ns)
et = AndOutput

setOutputs
  :: forall ns
  .  (PCA9685Outputs ns)
  => I2Cfd
  -> PCA9685Config
  -> SetOutputs ns
  -> IO ()
setOutputs fd config outputs
  = runI2CTransaction fd $ setOutputsTransaction config outputs

setOutputsTransaction
  :: forall ns
  .  (PCA9685Outputs ns)
  => PCA9685Config
  -> SetOutputs ns
  -> I2CTransaction ()
setOutputsTransaction config NoOutputs = pure ()
setOutputsTransaction config (AndOutput output outputs)
  = setOutputTransaction config output *> setOutputsTransaction config outputs

setOutputTransaction
  :: forall n
  .  (PCA9685Output n)
  => PCA9685Config
  -> SetOutput n
  -> I2CTransaction ()
setOutputTransaction config (SetOutput x) =
  writeToSlave
    (pca9685Address config)
    setRegisterString
  where
    setRegisterString =
         toStrict
      $  toLazyByteString
      $  word8 (onLRegister @ n)
      <> word16LE ledOnValue
      <> word16LE ledOffValue
    duration = truncate $ x * 4096
    x1Difference = 1 - x
    (ledOnValue, ledOffValue) =
      if x1Difference < 1/4096
      then alwaysOnValues
      else
        if x < 1/4096 
        then alwaysOffValues
        else proportionalValues
    alwaysOnValues = (setBit zeroBits 12, zeroBits)
    alwaysOffValues = (zeroBits, setBit zeroBits 12)
    proportionalValues =
      let
        onTime = pca9685PhaseSeparation config * (natValNum @ n)
        offTime = (onTime + duration) `mod` 4096
      in (onTime, offTime)

init
  :: I2Cfd
  -> PCA9685Config
  -> OutputType
  -> Bool
  -> IO ()
init fd config outputType invert = do
    runI2CTransaction fd
  $ writeToSlave (pca9685Address config)
    initString
  where
    initString =
         toStrict
      $  toLazyByteString
      $  word8 mode2
      <> word8 modeWord
    modeWord =
      outputTypeBit $ invertBit $ zeroBits
    outputTypeBit =
      case outputType of
        TotemPole -> flip setBit 2
        OpenDrain -> flip clearBit 2
    invertBit =
      if invert then flip setBit 4 else flip clearBit 4

natValNum :: forall n t . (KnownNat n, Num t) => t
natValNum = fromInteger $ natVal (Proxy :: Proxy n) 

